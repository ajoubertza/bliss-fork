# Also used by the CI to check source code with black and flake

python >= 3.7, < 3.10
black == 22.3
flake8
pre-commit
