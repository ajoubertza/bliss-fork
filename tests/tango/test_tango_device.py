import gevent
import weakref
from bliss.common.tango import DeviceProxy
import gc
import sys


def test_deviceproxy_release(lima_simulator):
    name = lima_simulator[0]
    d = DeviceProxy(name)
    proxy = weakref.ref(d)
    real_proxy = weakref.ref(d.__wrapped__)

    str(d), repr(d)  # <- This use to alloc unreleased ref count in PyTango
    d = None
    gevent.sleep(0.5)

    def debug_msg(obj):
        if obj is None:
            return "Finally obj was released"
        count = sys.getrefcount(obj)
        referrers = gc.get_referrers(obj)
        return f"Ref counts: {count}, referrers {referrers}"

    assert proxy() is None, debug_msg(proxy())
    gc.collect()
    assert real_proxy() is None, debug_msg(real_proxy())
