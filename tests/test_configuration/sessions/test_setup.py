import random  # noqa: F401
import math  # noqa: F401
import numpy
import gevent  # noqa: F401

from bliss import current_session
from bliss.shell.standard import *  # noqa: F403
from bliss.common.counter import SamplingCounter  # noqa: F401
from bliss.common.event import dispatcher  # noqa: F401
from bliss.scanning.scan_display import ScanDisplay
from bliss.common.utils import get_open_ports  # noqa: F401
from bliss.shell.standard import goto_custom, find_position

# deactivate automatic Flint startup
ScanDisplay().auto = False

load_script("script1")  # noqa: F405

SESSION_NAME = current_session.name

# Do not remove this print (used in tests)
print("TEST_SESSION INITIALIZED")
#


def special_com(x, y):
    return numpy.average(x, weights=y)


def find_special():
    return find_position(special_com)


def goto_special():
    return goto_custom(special_com)
