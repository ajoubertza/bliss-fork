# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy
import pytest


class RowlandCircle:
    def __init__(
        self,
        _bragg_angle,
        _rowlc_radius=500,
        _d_size=[-14, -14, -14, -14, -14],
        _n_analysers=5,
        _aradius=[500, 500, 500, 500, 500],
        _asym=-5.04931,
    ):

        self.Bragg = numpy.radians(_bragg_angle)
        self.asym = numpy.radians(_asym)

        self.R = _rowlc_radius
        self.R2 = _aradius

        self.X = [1, 0, 0]
        self.Y = [0, 1, 0]
        self.Z = [0, 0, 1]

        self.Ax = list(
            map(lambda x: 2 * x * numpy.sin(self.Bragg - self.asym), self.R2)
        )
        self.Ay = [0, 0, 0, 0, 0]
        self.Az = [0, 0, 0, 0, 0]

        self.Analyzer0 = numpy.array([self.Ax, self.Ay, self.Az])

        self.Dx = list(
            map(
                lambda x: 2
                * x
                * numpy.cos(self.Bragg + self.asym)
                * numpy.sin(2 * self.Bragg),
                self.R2,
            )
        )
        self.Dy = [0, 0, 0, 0, 0]
        self.Dz = list(
            map(
                lambda x: 2
                * x
                * numpy.sin(self.Bragg + self.asym)
                * numpy.sin(2 * self.Bragg),
                self.R2,
            )
        )

        self.Detector0 = numpy.array([self.Dx, self.Dy, self.Dz])

        self.Rx = list(map(lambda x: x * numpy.sin(self.Bragg - self.asym), self.R2))
        self.Ry = [0, 0, 0, 0, 0]
        self.Rz = list(map(lambda x: x * numpy.cos(self.Bragg - self.asym), self.R2))

        self.RowlandCenter0 = numpy.array([self.Rx, self.Ry, self.Rz])

        self.Detector = numpy.ndarray([_n_analysers, 3])
        self.Analyzer = numpy.ndarray([_n_analysers, 3])
        self.RowlandCenter = numpy.ndarray([_n_analysers, 3])

        self.Analyzer_prime = numpy.ndarray([_n_analysers, 3])
        self.AnalNorm = numpy.ndarray([_n_analysers, 3])

        psi, eta = self.psieta_calc(_d_size, _n_analysers, _rowlc_radius, _aradius)

        self.RowlandCircleCoordinates_calc(psi, eta, _aradius, _n_analysers)

    def psieta_calc(self, _d_size, _n_analysers, _rowlc_radius, _anal_radius):

        if _rowlc_radius != 500 and _rowlc_radius != 1000:
            raise ValueError(f"Unknown Radius {_rowlc_radius}")

        if _n_analysers == 1:
            ii = numpy.array([0.0])
        else:
            ii = numpy.arange(_n_analysers, dtype=int)
            ii = ii - int(_n_analysers / 2)

        d_y = ii * _d_size

        alpha = (ii / _rowlc_radius) * 3500
        self.angular_offset = alpha
        self.detector_offset = d_y
        alpha = list(map(numpy.radians, alpha))
        self.alpha = alpha

        psi = list(map(lambda x: -numpy.arcsin(x / self.Dz[2]), d_y))
        # psi = -numpy.arcsin(d_y/self.Dz[2])
        eta = self.eta_calc(ii, alpha, psi)

        self.psi = psi
        self.eta = eta

        return psi, eta

    def eta_calc(self, ii, alpha, psi):
        eta = list(
            map(
                lambda dx, dz, _psi, _alpha: (
                    numpy.sin(_psi) ** 2 * dx**2 * dz**2
                    + numpy.sin(_psi) * dx**3 * dz * numpy.tan(_alpha)
                    - numpy.sin(_psi) * dx * dz**3 * numpy.tan(_alpha)
                    - dx**2 * dz**2 * numpy.tan(_alpha) ** 2
                    + numpy.abs(dz)
                    * numpy.cos(_psi)
                    * (dx**2 + dz**2)
                    * numpy.sqrt(
                        -2 * numpy.sin(_psi) * dx * dz * numpy.tan(_alpha)
                        - dx**2 * numpy.tan(_alpha) ** 2
                        + dz**2 * (numpy.cos(_psi) ** 2 + numpy.tan(_alpha) ** 2)
                    )
                )
                / (
                    dz**2
                    * (
                        dx**2
                        - 2 * numpy.sin(_psi) * dx * dz * numpy.tan(_alpha)
                        + dz**2 * (numpy.cos(_psi) ** 2 + numpy.tan(_alpha) ** 2)
                    )
                ),
                self.Dx,
                self.Dz,
                psi,
                alpha,
            )
        )
        eta = list(map(lambda x: numpy.round(x, 15), eta))
        eta = list(map(numpy.arccos, eta))
        eta = list(map(lambda x, y: numpy.sign(x) * y, ii, eta))
        return eta

    def RotationMatrix(self, versor, theta):

        versorr = versor

        norm = numpy.sqrt(versorr[0] ** 2 + versorr[1] ** 2 + versorr[2] ** 2)

        if norm != 1:
            versorr[0] = versorr[0] / norm
            versorr[1] = versorr[1] / norm
            versorr[2] = versorr[2] / norm

        a = [
            versorr[0] ** 2 + (1 - versorr[0] ** 2) * numpy.cos(theta),
            (1 - numpy.cos(theta)) * versorr[0] * versorr[1]
            - numpy.sin(theta) * versorr[2],
            (1 - numpy.cos(theta)) * versorr[0] * versorr[2]
            + numpy.sin(theta) * versorr[1],
        ]

        b = [
            (1 - numpy.cos(theta)) * versorr[0] * versorr[1]
            + numpy.sin(theta) * versorr[2],
            versorr[1] ** 2 + (1 - versorr[1] ** 2) * numpy.cos(theta),
            (1 - numpy.cos(theta)) * versorr[1] * versorr[2]
            - numpy.sin(theta) * versorr[0],
        ]

        c = [
            (1 - numpy.cos(theta)) * versorr[0] * versorr[2]
            - numpy.sin(theta) * versorr[1],
            (1 - numpy.cos(theta)) * versorr[1] * versorr[2]
            + numpy.sin(theta) * versorr[0],
            versorr[2] ** 2 + (1 - versorr[2] ** 2) * numpy.cos(theta),
        ]

        res = [a, b, c]
        return numpy.array(res)

    def RowlandCircleCoordinates_calc(self, psi, eta, _anal_radius, _n_analysers):

        for nn in range(_n_analysers):
            self.Detector[nn, :] = numpy.dot(
                self.RotationMatrix(self.X, psi[nn]), self.Detector0[:, nn].T
            )
            det = numpy.copy(self.Detector[nn, :])

            self.Analyzer[nn, :] = numpy.dot(
                self.RotationMatrix(det, eta[nn]), self.Analyzer0[:, nn].T
            )
            self.Analyzer_prime[nn, :] = numpy.dot(
                self.RotationMatrix(self.Z, -self.alpha[nn]), self.Analyzer[nn, :]
            )

            self.RowlandCenter[nn, :] = numpy.dot(
                self.RotationMatrix(det, eta[nn]),
                numpy.dot(
                    self.RotationMatrix(self.X, psi[nn]),
                    self.RowlandCenter0[:, nn].T,
                ),
            )

            self.AnalNorm[nn, :] = -numpy.dot(
                self.RotationMatrix(det, eta[nn]),
                numpy.dot(
                    self.RotationMatrix(self.X, psi[nn]),
                    numpy.dot(
                        self.RotationMatrix(
                            self.Y, numpy.pi / 2 - self.Bragg + self.asym
                        ),
                        self.X,
                    ),
                ),
            )
            self.AnalNorm[nn, :] = self.AnalNorm[nn, :] / numpy.sqrt(
                self.AnalNorm[nn, 0] ** 2
                + self.AnalNorm[nn, 1] ** 2
                + self.AnalNorm[nn, 2] ** 2
            )


class RIXS:
    def __init__(
        self,
        _rowlc_radius=500,
        _d_size=[-14, -14, -14, -14, -14],
        _n_analysers=5,
        _beta=21,
        _aradius=[500, 500, 500, 500, 500],
        _asym=-5.04931,
    ):
        self.R = _rowlc_radius
        self.R2 = _aradius
        self.dsize = _d_size
        self.n_analysers = _n_analysers
        self.beta = _beta
        self.asym = _asym

    def cart2sph(self, x, y, z):
        XsqPlusYsq = x**2 + y**2
        r = numpy.sqrt(XsqPlusYsq + z**2)  # r
        elev = numpy.arctan(z / numpy.sqrt(XsqPlusYsq))  # theta
        az = numpy.arctan(y / x)  # phi

        return (az, elev, r)

    def getPositions(self, _bragg_angle):

        self.rc = RowlandCircle(
            _bragg_angle, self.R, self.dsize, self.n_analysers, self.R2, self.asym
        )

        self.spherical = list(
            map(
                self.cart2sph,
                -self.rc.AnalNorm.T[0],
                -self.rc.AnalNorm.T[1],
                self.rc.AnalNorm.T[2],
            )
        )
        self.spherical = numpy.array(self.spherical)

        # self.atheta = map (degrees,pi/2-self.spherical[:,1]-numpy.radians(self.asym))
        self.atheta = list(map(numpy.degrees, numpy.pi / 2 - self.spherical[:, 1]))

        self.achi = list(map(lambda x, y: x - y, self.spherical[:, 0], self.rc.alpha))

        self.achim = list(
            map(
                lambda x: 47.5 * numpy.tan(x + numpy.arctan(10.82 / 47.5)) - 10.82,
                self.achi,
            )
        )
        self.achi = list(map(numpy.degrees, self.achi))
        self.ax = list(
            map(
                lambda x, y: (y / numpy.cos(x)).tolist(),
                self.rc.alpha,
                self.rc.Analyzer.T[0],
            )
        )

        ix = int(self.n_analysers / 2)
        beta = numpy.radians(self.beta)

        self.dx = self.rc.Detector[ix, 0] * numpy.cos(beta) - self.rc.Detector[
            ix, 2
        ] * numpy.sin(beta)
        self.dz = self.rc.Detector[ix, 0] * numpy.sin(beta) + self.rc.Detector[
            ix, 2
        ] * numpy.cos(beta)

        self.detnorm = self.rc.Detector0[:, 2] - self.rc.RowlandCenter0[:, 2]
        self.detnorm = self.detnorm / numpy.sqrt(
            self.detnorm[0] ** 2 + self.detnorm[1] ** 2 + self.detnorm[2] ** 2
        )

        self.drot = numpy.degrees(
            numpy.arccos(numpy.dot(self.detnorm, numpy.array(self.rc.X)))
        )

        results = {}
        results["Ai"] = self.rc.Analyzer
        results["Di"] = self.rc.Detector
        results["Ri"] = self.rc.RowlandCenter
        results["Nia"] = self.rc.AnalNorm

        results["psi"] = self.rc.psi
        results["eta"] = self.rc.eta

        results["spherical"] = self.spherical
        results["achi"] = self.achi
        results["ax"] = self.ax
        results["dx"] = self.dx
        results["dz"] = self.dz
        results["drot"] = self.drot

        results["params"] = {
            "rowland": self.R,
            "radius": self.R2,
            "dsize": self.dsize,
            "beta": self.beta,
            "asym": self.asym,
            "angular_offset": self.rc.angular_offset,
            "detector_offset": self.rc.detector_offset,
        }

        return results


def _xes_eh2(radius, bragg, a_y_off, a_z_set, det_deviation_x, miscut):

    # Notes about convention difference:
    #  radius = 2 * radius
    #  miscut = - miscut
    #  det_deviation != offset_on_detector
    #  offset_on_detector = 0 (for mmspectro)

    def _make_angles(vector1, vector2):
        Unit_vector1 = vector1 / numpy.linalg.norm(vector1)
        Unit_vector2 = vector2 / numpy.linalg.norm(vector2)
        dot_product = numpy.dot(Unit_vector1, Unit_vector2)
        angle = numpy.arccos(dot_product)
        return angle

    numpy.set_printoptions(precision=3, suppress="True")

    radius = 2 * radius
    theta = numpy.deg2rad(bragg)  # convert angles in radians
    alpha = numpy.deg2rad(-miscut)

    # ____caculate positions in Rowland frame assuming that detector and sample are placed along vertical line_____
    axvert = radius * numpy.square(
        numpy.sin(theta)
    )  # distance center line sample-detector and symmetric crystal Rowland position
    azvert = radius * numpy.sin(theta) * numpy.cos(theta)
    dzvert = 2 * azvert  # distance sample - detector

    Avec = [
        radius * numpy.sin(theta + alpha) * numpy.sin(theta - alpha),
        0,
        radius * numpy.sin(theta + alpha) * numpy.cos(theta - alpha),
    ]
    Dvec = [0, 0, dzvert]
    Rvec = [
        axvert,
        0,
        azvert,
    ]  # this is needed to define the position of the Rowland circle

    rot_alpha = numpy.array(
        [
            [numpy.cos(-alpha), 0, numpy.sin(-alpha)],
            [0, 1, 0],
            [-numpy.sin(-alpha), 0, numpy.cos(-alpha)],
        ]
    )
    CvecNorm = rot_alpha.dot([-1, 0, 0])  # norm on crystal plane
    AvecNorm = rot_alpha.dot(CvecNorm)  # norm on crystal optical surface
    RvecNorm = [-1, 0, 0]

    y_angle = numpy.arcsin(a_y_off / Avec[0])  # phi rotation in vertical position
    rot_z = numpy.array(
        [
            [numpy.cos(y_angle), -numpy.sin(y_angle), 0],
            [numpy.sin(y_angle), numpy.cos(y_angle), 0],
            [0, 0, 1],
        ]
    )
    Avec = rot_z.dot(Avec)
    Dvec = rot_z.dot(Dvec)
    Rvec = rot_z.dot(Rvec)
    CvecNorm = rot_z.dot(CvecNorm)
    AvecNorm = rot_z.dot(AvecNorm)
    RvecNorm = rot_z.dot(RvecNorm)

    # rotate Rowland frame such that az becomes a_z_set defined as
    # position of center of analyzer crystal
    if (
        a_z_set != -1
    ):  # choose -1 if you do not want to rotate around y axis. This is only for testing and not an option in the EH2 spectrometer
        theta_z_off = numpy.arcsin(a_z_set / radius * numpy.sin(theta + alpha))
        z_angle = (
            numpy.pi / 2 - theta + alpha - theta_z_off
        )  # rotation to have crystal at zoffvec(1). Positive alpha gives positive azoff
        rot_y = numpy.array(
            [
                [numpy.cos(z_angle), 0, numpy.sin(z_angle)],
                [0, 1, 0],
                [-numpy.sin(z_angle), 0, numpy.cos(z_angle)],
            ]
        )
        Avec = rot_y.dot(Avec)
        Dvec = rot_y.dot(Dvec)
        Rvec = rot_y.dot(Rvec)
        CvecNorm = rot_y.dot(CvecNorm)
        AvecNorm = rot_y.dot(AvecNorm)
        RvecNorm = rot_y.dot(RvecNorm)

    DvecNorm = numpy.subtract(Avec, Dvec)
    DvecNorm = DvecNorm / numpy.linalg.norm(DvecNorm)

    theta_ana = _make_angles(AvecNorm, [0, 0, 1])
    chi_ana = _make_angles([AvecNorm[0], AvecNorm[1], 0], [-1, 0, 0]) * numpy.sign(
        AvecNorm[1]
    )  # angle in the xy plane
    dth = _make_angles(DvecNorm, [-1, 0, 0])

    # apply deviation of detector position. Definition in agreement with ID26 ray tracing code
    # print('det_deviation_x',det_deviation_x)
    det_deviation = det_deviation_x / numpy.cos(numpy.pi - dth)
    Dvec = Dvec + DvecNorm * det_deviation

    return Avec, Dvec, Rvec, theta_ana, chi_ana, dth


def compare_rix(
    spectro,
    bragg_angle,
    asym,
    dsize=14,
    radius=500,
    crossing=True,
    rtol=1e-6,
    n_analysers=5,
):

    analid = {"ana_0": 2, "ana_1": 1, "ana_2": 3, "ana_3": 0, "ana_4": 4}

    # negative dsize means crossing geometry
    if crossing:
        dsize = -numpy.abs(dsize)
    else:
        dsize = numpy.abs(dsize)

    # compute RIX solution
    rixs = RIXS(
        _rowlc_radius=radius,
        _d_size=n_analysers * [dsize],
        _n_analysers=n_analysers,
        _aradius=n_analysers * [radius],
        _asym=asym,
    )
    rix_res = rixs.getPositions(bragg_angle)

    # prepare spectro like RIX
    spectro.referential_origin = 0, 0, 0
    for ana in spectro._active_analysers:
        anid = analid[ana.name]
        ana.miscut = rix_res["params"]["asym"]
        ana.radius = rix_res["params"]["radius"][anid]
        ana.angular_offset = rix_res["params"]["angular_offset"][anid]
        ana.offset_on_detector = rix_res["params"]["detector_offset"][anid]

    spectro.bragg_axis.move(bragg_angle)

    print(spectro.__info__())

    assert spectro.is_aligned == (True, True)

    for ana in spectro._active_analysers:
        bragg, solution, _ = ana._current_bragg_solution
        assert numpy.isclose(bragg_angle, ana.bragg_axis.position, rtol=rtol)
        assert numpy.isclose(bragg_angle, bragg, rtol=rtol)
        anid = analid[ana.name]
        assert numpy.all(
            numpy.isclose(solution["Ai"], rix_res["Ai"][anid, :], rtol=rtol)
        )
        assert numpy.all(
            numpy.isclose(solution["Di"], rix_res["Di"][anid, :], rtol=rtol)
        )
        assert numpy.all(
            numpy.isclose(solution["Ri"], rix_res["Ri"][anid, :], rtol=rtol)
        )
        assert numpy.all(
            numpy.isclose(solution["Nia"], rix_res["Nia"][anid, :], rtol=rtol)
        )


def compare_mmspectrometer(spectro, bragg_angle, asym, dsize=14, radius=500, rtol=1e-6):

    spectro.referential_origin = 0, 0, 0

    a_z_set = 0
    det_deviation_x = 0
    mmres = {}
    for ana in spectro._active_analysers:
        a_y_off = ana.ypos
        mmres[ana.name] = _xes_eh2(
            radius, bragg_angle, a_y_off, a_z_set, det_deviation_x, asym
        )

    # prepare spectro like RIX
    for ana in spectro._active_analysers:
        ana.miscut = asym
        ana.radius = radius
        ana.offset_on_detector = 0  # mmspectro is det_0D

    spectro.bragg_axis.move(bragg_angle)

    print(spectro.__info__())

    assert spectro.is_aligned == (True, True)

    for ana in spectro._active_analysers:
        bragg, solution, _ = ana._current_bragg_solution
        assert numpy.isclose(bragg_angle, ana.bragg_axis.position, rtol=rtol)
        assert numpy.isclose(bragg_angle, bragg, rtol=rtol)
        Avec, Dvec, Rvec, theta_ana, chi_ana, dth = mmres[ana.name]
        assert numpy.all(numpy.isclose(solution["Ai"], Avec, rtol=rtol))
        assert numpy.all(numpy.isclose(solution["Di"], Dvec, rtol=rtol))


def test_validate_cylindrical_geometry_vs_rix(beacon):
    """Compare new spectrometer cylindrical geometry with the RIX spectrometer results"""

    spectro = beacon.get("spectro")
    rtol = 1e-6
    asym = -5.04931
    dsize = 14
    crossing = True
    radius = 500
    bragg_angle = 85.5266649413
    # 11.218 keV <=> 85.5266649413 => 'optical' incident angle = bragg - asym = 90.5759749413
    for bragg_angle in range(60, 90, 5):
        for asym in range(0, 6, 1):
            compare_rix(spectro, bragg_angle, -asym, dsize, radius, crossing, rtol)

    # clean Louie connections
    spectro.__close__()


def test_validate_cartesian_geometry_vs_mmspectrometer(beacon):
    """Compare new spectrometer cartesian geometry with the  mmspectrometer results"""

    spectro = beacon.get("spectro_cr")
    rtol = 1e-6
    asym = -5.04931
    dsize = 14
    radius = 500
    bragg_angle = 85.5266649413
    # 11.218 keV <=> 85.5266649413 => 'optical' incident angle = bragg - asym = 90.5759749413

    compare_mmspectrometer(spectro, bragg_angle, asym, dsize, radius, rtol)

    for bragg_angle in range(60, 90, 5):
        for asym in range(0, 6, 1):
            compare_mmspectrometer(spectro, bragg_angle, -asym, dsize, radius, rtol)

    # clean Louie connections
    spectro.__close__()


def test_validate_mmspectrometer(beacon):
    """Compare new spectrometer cartesian geometry with the  mmspectrometer results"""

    spectro = beacon.get("spectro_mm")
    rtol = 1e-6
    asym = -5.04931
    dsize = 14
    radius = 500
    bragg_angle = 85.5266649413
    # 11.218 keV <=> 85.5266649413 => 'optical' incident angle = bragg - asym = 90.5759749413

    compare_mmspectrometer(spectro, bragg_angle, asym, dsize, radius, rtol)

    for bragg_angle in range(60, 90, 5):
        for asym in range(0, 6, 1):
            compare_mmspectrometer(spectro, bragg_angle, -asym, dsize, radius, rtol)

    # clean Louie connections
    spectro.__close__()


def test_spectrometer_api(beacon):
    s = beacon.get("spectro")

    # activate flint plot
    # s.plot

    # set spectro origin in lab ref
    s.referential_origin = 0, 0, 0

    # check 5 analysers in config
    assert len(s.analysers) == 5
    assert [ana.name for ana in s.analysers] == [
        "ana_0",
        "ana_1",
        "ana_2",
        "ana_3",
        "ana_4",
    ]
    assert s.detector.name == "det_0"

    # check axes naming
    assert list(s.bragg_calc_controller.axes.keys()) == [
        "bragg_spectro",
        "bragg_det_0",
        "bragg_ana_0",
        "bragg_ana_1",
        "bragg_ana_2",
        "bragg_ana_3",
        "bragg_ana_4",
    ]
    assert list(s.bragg_calc_controller._tagged.keys()) == [
        "bragg",
        "real",
        "bragg_det_0",
        "bragg_ana_0",
        "bragg_ana_1",
        "bragg_ana_2",
        "bragg_ana_3",
        "bragg_ana_4",
    ]
    assert list(s.energy_calc_controller.axes.keys()) == [
        "ene_spectro",
        "ene_det_0",
        "ene_ana_0",
        "ene_ana_1",
        "ene_ana_2",
        "ene_ana_3",
        "ene_ana_4",
    ]
    assert list(s.energy_calc_controller._tagged.keys()) == [
        "energy",
        "real",
        "energy_det_0",
        "energy_ana_0",
        "energy_ana_1",
        "energy_ana_2",
        "energy_ana_3",
        "energy_ana_4",
    ]

    assert list(s.analysers.ana_0.bragg_calc_controller.axes.keys()) == [
        "bragg_ana_0",
        "xa0",
        "za0",
        "pita0",
        "yawa0",
    ]
    assert list(s.analysers.ana_0.bragg_calc_controller._tagged.keys()) == [
        "bragg",
        "real",
        "rpos",
        "zpos",
        "pitch",
        "yaw",
    ]

    assert list(s.analysers.ana_0.energy_calc_controller.axes.keys()) == [
        "ene_ana_0",
        "bragg_ana_0",
    ]
    assert list(s.analysers.ana_0.energy_calc_controller._tagged.keys()) == [
        "energy",
        "real",
        "bragg",
    ]

    assert list(s.detector.bragg_calc_controller.axes.keys()) == [
        "bragg_det_0",
        "xdet",
        "zdet",
        "pitdet",
    ]
    assert list(s.detector.bragg_calc_controller._tagged.keys()) == [
        "bragg",
        "real",
        "xpos",
        "zpos",
        "pitch",
    ]

    assert list(s.detector.energy_calc_controller.axes.keys()) == [
        "ene_det_0",
        "bragg_det_0",
    ]
    assert list(s.detector.energy_calc_controller._tagged.keys()) == [
        "energy",
        "real",
        "bragg",
    ]

    # activate all analysers
    s.unfreeze(*s.frozen)
    assert len(s._active_analysers) == 5
    assert [ana.name for ana in s._active_analysers] == [
        "ana_0",
        "ana_1",
        "ana_2",
        "ana_3",
        "ana_4",
    ]

    # move to bragg = 70
    s.bragg_axis.move(70)
    assert s.is_aligned == (True, True)
    assert s.bragg_axis.position == 70
    assert numpy.isclose(s.analysers.ana_0.geo_bragg, 70, rtol=1e-6)

    # try to freeze detector target
    with pytest.raises(ValueError):
        s.freeze(s.analysers.ana_0)

    # freeze a2 and a3
    s.freeze("ana_2", "ana_3")
    assert len(s._active_analysers) == 3
    assert [ana.name for ana in s._active_analysers] == ["ana_0", "ana_1", "ana_4"]

    # set crystal Si844
    for ana in s._active_analysers:
        ana.xtal_sel = "Si844"

    # check energy <=> bragg conversion
    #   11.218 keV <=> 85.5266649413 degree
    for ana in s._active_analysers:
        assert ana.energy_calc_controller.dspacing == 5.543013043019761e-11
    s.energy_axis.move(11.218)
    assert numpy.isclose(s.bragg_axis.position, 85.5266649413, rtol=1e-6)

    # move to bragg = 80
    s.bragg_axis.move(80)
    assert s.is_aligned == (True, True)
    assert s.bragg_axis.position == 80
    assert numpy.isclose(s.analysers.ana_0.geo_bragg, 80, rtol=1e-6)

    # clean Louie connections
    s.__close__()


def test_spectrometer_cartesian(beacon):
    s = beacon.get("spectro_cr")
    # change sample ref pos
    s.referential_origin = 500, 0, 200

    assert s.detector.target == s.analysers.ana_0_cr

    # move to bragg = 80
    s.bragg_axis.move(80)
    assert s.is_aligned == (True, True)
    assert s.bragg_axis.position == 80
    assert numpy.isclose(s.analysers.ana_0_cr.geo_bragg, 80, rtol=1e-6)

    # set_energy
    ene = s.energy_axis.position
    theo_reals = s.analysers.ana_0_cr._current_bragg_solution[2]
    s.analysers.ana_0_cr.set_energy(ene, interactive=False)
    for k, v in theo_reals.items():
        assert numpy.isclose(s.analysers.ana_0_cr.real_axes[k].position, v, rtol=1e-4)

    # clean Louie connections
    s.__close__()
