import pytest
import gevent
from bliss.common.shutter import BaseShutterState
from bliss.testutils.rpc_controller import controller_in_another_process


def test_mockupshutter_no_config(beacon):
    """
    Create a mockupshutter

    Check that the state is unknown
    """
    shutter = beacon.get("mockupshutter")
    assert shutter.state == BaseShutterState.UNKNOWN


def test_mockupshutter_wrong_config(beacon):
    """
    Create a mockupshutter with wrong config

    Check that an exception is rasied containing the wrong property
    """
    with pytest.raises(RuntimeError) as exc_info:
        beacon.get("mockupshutter_wrong_config")
    assert "foobar" in exc_info.value.args[0]
    assert "state" not in exc_info.value.args[0]


def test_mockupshutter_config_closed(beacon):
    """
    Create a mockupshutter closed

    Check that the state is closed
    """
    shutter = beacon.get("mockupshutter_closed")
    assert shutter.state == BaseShutterState.CLOSED


def test_mockupshutter_open(beacon):
    """
    Create a shutter and open it

    Check that the motor is open
    """
    shutter = beacon.get("mockupshutter")
    assert shutter.state != BaseShutterState.OPEN
    shutter.open()
    assert shutter.state == BaseShutterState.OPEN


def test_mockupshutter_close(beacon):
    """
    Create a shutter and open it

    Check that the motor is closed
    """
    shutter = beacon.get("mockupshutter")
    assert shutter.state != BaseShutterState.CLOSED
    shutter.close()
    assert shutter.state == BaseShutterState.CLOSED


def test_mockupshutter_open_and_close(beacon):
    """
    Open and close the shutter

    Check that the state is the one from the last action
    """
    shutter = beacon.get("mockupshutter")
    shutter.open()
    shutter.close()
    assert shutter.state == BaseShutterState.CLOSED


def test_mockupshutter_slow_open(beacon):
    """
    Open a shutter which is slow

    Check that the state tasks time to be open
    """
    shutter = beacon.get("mockupshutter_slow_open")
    assert shutter.state != BaseShutterState.OPEN
    shutter.open(timeout=0.1)
    assert shutter.state != BaseShutterState.OPEN
    gevent.sleep(0.1)
    assert shutter.state != BaseShutterState.OPEN
    gevent.sleep(1)
    assert shutter.state == BaseShutterState.OPEN


@pytest.fixture
def mockupshutter_in_another_process(beacon, beacon_host_port):
    beacon_host, beacon_port = beacon_host_port
    with controller_in_another_process(beacon_host, beacon_port, "mockupshutter") as c:
        yield c


def test_mockupshutter_with_2_sessions(beacon, mockupshutter_in_another_process):
    """Create the same shutter controller in 2 sessions.

    Check that a change from one is synchronized in another one.
    """
    remote_shutter = mockupshutter_in_another_process
    local_shutter = beacon.get("mockupshutter")

    local_shutter.open()
    gevent.sleep(0.5)
    assert local_shutter.state == BaseShutterState.OPEN
    assert remote_shutter.state == BaseShutterState.OPEN

    local_shutter.close()
    gevent.sleep(0.5)
    assert local_shutter.state == BaseShutterState.CLOSED
    assert remote_shutter.state == BaseShutterState.CLOSED

    remote_shutter.open()
    gevent.sleep(0.5)
    assert remote_shutter.state == BaseShutterState.OPEN
    assert local_shutter.state == BaseShutterState.OPEN

    remote_shutter.close()
    gevent.sleep(0.5)
    assert remote_shutter.state == BaseShutterState.CLOSED
    assert local_shutter.state == BaseShutterState.CLOSED


def test_mockupshutter_with_2_sessions_single_state_inititialization(
    beacon, mockupshutter_in_another_process
):
    """Create the same shutter controller in 2 sessions.

    Make sure the second instanciation does not rest the state
    """
    remote_shutter = mockupshutter_in_another_process
    assert remote_shutter != BaseShutterState.OPEN
    remote_shutter.open()

    gevent.sleep(0.5)
    local_shutter = beacon.get("mockupshutter")
    assert local_shutter.state == BaseShutterState.OPEN
