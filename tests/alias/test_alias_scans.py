from bliss.common import scans


def test_scan_info_display_names_with_alias(alias_session):
    robyy = alias_session.env_dict["robyy"]
    dtime = alias_session.env_dict["dtime"]
    diode = alias_session.config.get("diode")
    toto = alias_session.env_dict["ALIASES"].add("toto", diode)

    s = scans.ascan(robyy, 0, 1, 3, 0.1, dtime, toto, run=False)

    acq_chan = s.acq_chain.nodes_list[0].channels[0]
    assert acq_chan.name == "axis:robyy"
    assert (
        "axis:" + s.scan_info["channels"][acq_chan.fullname]["display_name"]
        == acq_chan.name
    )
    dtime_chan = s.acq_chain.nodes_list[-2].channels[0]
    assert (
        s.scan_info["channels"][dtime_chan.fullname]["display_name"] == dtime_chan.name
    )
    toto_chan = s.acq_chain.nodes_list[-1].channels[0]
    assert s.scan_info["channels"][toto_chan.fullname]["display_name"] == toto_chan.name


def test_alias_scan_title(alias_session):
    env_dict = alias_session.env_dict

    robyy = env_dict["robyy"]
    m1 = env_dict["m1"]
    mot0 = env_dict["mot0"]
    diode = alias_session.config.get("diode")

    s = scans.ascan(robyy, 0, 1, 3, 0.1, diode, run=False)
    assert "ascan" in s.scan_info["type"]
    assert "robyy" in s.scan_info["title"]

    s = scans.dmesh(robyy, 0, 1, 3, m1, 0, 1, 3, 0.1, diode, run=False)
    assert "dmesh" in s.scan_info["type"]
    assert "robyy" in s.scan_info["title"]
    assert "m1" in s.scan_info["title"]

    s = scans.a2scan(robyy, 0, 1, m1, 0, 1, 3, 0.1, diode, run=False)
    assert "a2scan" in s.scan_info["type"]
    assert "robyy" in s.scan_info["title"]
    assert "m1" in s.scan_info["title"]

    s = scans.d2scan(robyy, 0, 1, m1, 0, 1, 3, 0.1, diode, run=False)
    assert "d2scan" in s.scan_info["type"]
    assert "robyy" in s.scan_info["title"]
    assert "m1" in s.scan_info["title"]

    # starting from 3, the underlying scan function is 'anscan',
    # so it does not need to test aNscan,dNscan with N>3 it is all the
    # same code
    s = scans.a3scan(robyy, 0, 1, m1, 0, 1, mot0, 0, 1, 3, 0.1, diode, run=False)
    assert "a3scan" in s.scan_info["type"]
    assert "robyy" in s.scan_info["title"]
    assert "m1" in s.scan_info["title"]
    assert "mot0" in s.scan_info["title"]

    s = scans.d3scan(robyy, 0, 1, m1, 0, 1, mot0, 0, 1, 3, 0.1, diode, run=False)
    assert "d3scan" in s.scan_info["type"]
    assert "robyy" in s.scan_info["title"]
    assert "m1" in s.scan_info["title"]
    assert "mot0" in s.scan_info["title"]
