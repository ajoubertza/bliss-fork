# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import pytest
import gevent
from bliss.scanning.scan_saving import set_scan_saving_class


@pytest.fixture
def beacon_with_logging(beacon, capsys, caplog, log_context, log_shell_mode):
    yield beacon


@pytest.fixture
def logging_session(beacon_with_logging):
    session = beacon_with_logging.get("test_logging_session")
    session.setup()
    yield session
    session.close()


@pytest.fixture
def beacon_with_logging_esrf(beacon_with_logging):
    scan_saving_cfg = beacon_with_logging.root["scan_saving"]
    scan_saving_cfg["class"] = "ESRFScanSaving"
    yield beacon_with_logging
    set_scan_saving_class(None)


@pytest.fixture
def logging_session_without_elogserver(beacon_with_logging_esrf, log_directory):
    logfile = os.path.join(log_directory, "test_logging_session.log")
    with open(logfile, "w"):
        pass
    session = beacon_with_logging_esrf.get("test_logging_session")
    session.setup()
    yield session
    session.close()


@pytest.fixture
def logging_session_with_elogserver(
    beacon_with_logging_esrf, icat_mock_client, log_directory
):
    logfile = os.path.join(log_directory, "test_logging_session.log")
    with open(logfile, "w"):
        pass
    session = beacon_with_logging_esrf.get("test_logging_session")
    session.setup()
    yield session
    session.close()


def check_scripts_finished(session):
    assert session.env_dict.get("setupfinished")
    assert session.env_dict.get("scriptfinished")


def check_user_logging(capsys, elog_offline=False, data_policy=True):
    captured = capsys.readouterr().err.split("\n")
    captured = [s for s in captured if s]
    nexpected = 6 + (elog_offline and data_policy)
    assert len(captured) == nexpected, captured
    i = 0
    if data_policy and elog_offline:
        expected = "WARNING: The ICAT client is disabled in the beacon configuration. Datasets will not be registered and e-logbook messages are lost."
        assert captured[i] == expected
        i += 1
    expected = "ERROR: LogInitController: user error"
    assert captured[i] == expected
    i += 1
    expected = "LogInitController: Beacon error"
    assert expected in captured[i]
    i += 1
    expected = "ERROR: test_logging_session.py: user error"
    assert captured[i] == expected
    i += 1
    expected = "test_logging_session.py: Beacon error"
    assert expected in captured[i]
    i += 1
    expected = "ERROR: logscript.py: user error"
    assert captured[i] == expected
    i += 1
    expected = "logscript.py: Beacon error"
    assert expected in captured[i]


def check_beacon_logging(caplog, logfile, elog_offline=False):
    nrecords = 3

    # Get lines from logging capture
    records = caplog.get_records("setup")
    assert len(records) == nrecords, records

    # Get lines from the log server file
    try:
        lines = []
        with gevent.Timeout(10):
            while len(lines) != nrecords:
                with open(logfile, "r") as f:
                    lines = [line.rstrip() for line in f]
                gevent.sleep(0.5)
    except gevent.Timeout:
        assert len(lines) == nrecords, lines

    i = 0
    expected = "LogInitController: Beacon error"
    assert records[0].levelname == "ERROR"
    assert records[0].message == expected
    assert expected in lines[i]
    i += 1
    expected = "test_logging_session.py: Beacon error"
    assert records[i].levelname == "ERROR"
    assert records[i].message == expected
    assert expected in lines[i]
    i += 1
    expected = "logscript.py: Beacon error"
    assert records[i].levelname == "ERROR"
    assert records[i].message == expected
    assert expected in lines[i]


def check_elogging(icat_mock_client):
    assert icat_mock_client.return_value.send_message.call_count == 3
    assert icat_mock_client.return_value.send_message.called_once_with(
        "LogInitController: E-logbook error",
        beamline_only=None,
        msg_type="error",
        tags=["test_logging_session"],
    )
    assert icat_mock_client.return_value.send_message.called_once_with(
        "test_logging_session.py: E-logbook error",
        beamline_only=None,
        msg_type="error",
        tags=["test_logging_session"],
    )
    assert icat_mock_client.return_value.send_message.called_once_with(
        "logscript.py: E-logbook error",
        beamline_only=None,
        msg_type="error",
        tags=["test_logging_session"],
    )


def test_setup_logging_no_data_policy(
    logging_session, capsys, caplog, log_directory, icat_mock_client
):
    logfile = os.path.join(log_directory, logging_session.name + ".log")
    check_scripts_finished(logging_session)
    check_user_logging(capsys, data_policy=False)
    check_beacon_logging(caplog, logfile)
    assert icat_mock_client.call_count == 0


def test_setup_logging_without_elogserver(
    logging_session_without_elogserver,
    capsys,
    caplog,
    log_directory,
    icat_mock_client,
):
    logfile = os.path.join(
        log_directory, logging_session_without_elogserver.name + ".log"
    )
    check_scripts_finished(logging_session_without_elogserver)
    check_user_logging(capsys, elog_offline=True)
    check_beacon_logging(caplog, logfile, elog_offline=True)
    assert icat_mock_client.call_count == 0


def test_setup_logging_with_elogserver(
    logging_session_with_elogserver,
    capsys,
    caplog,
    log_directory,
    icat_mock_client,
):
    logfile = os.path.join(log_directory, logging_session_with_elogserver.name + ".log")
    check_scripts_finished(logging_session_with_elogserver)
    check_user_logging(capsys)
    check_beacon_logging(caplog, logfile)
    check_elogging(icat_mock_client)
