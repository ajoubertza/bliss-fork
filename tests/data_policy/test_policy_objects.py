import pytest
from blissdata.data.node import get_node
from bliss.shell.standard import newproposal, newsample, newdataset
from bliss.common.standard import loopscan
from . import icat_test_utils


def test_data_policy_objects(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    # Prepare for scanning without the Nexus writer
    diode = session.env_dict["diode"]
    scan_saving.writer = "hdf5"

    proposal = scan_saving.proposal
    collection = scan_saving.collection
    dataset = scan_saving.dataset

    assert proposal.node.type == "proposal"
    assert str(proposal) == scan_saving.proposal_name
    assert proposal.name == scan_saving.proposal_name
    assert proposal.has_samples
    assert len(list(proposal.sample_nodes)) == 1

    assert collection.node.type == "dataset_collection"
    assert str(collection) == scan_saving.collection_name
    assert collection.proposal.name == scan_saving.proposal_name
    assert collection.name == scan_saving.collection_name
    assert collection.has_datasets
    assert len(list(collection.dataset_nodes)) == 1
    assert collection.proposal.node.db_name == proposal.node.db_name

    assert dataset.node.type == "dataset"
    assert str(dataset) == scan_saving.dataset_name
    assert dataset.proposal.name == scan_saving.proposal_name
    assert dataset.collection.name == scan_saving.collection_name
    assert dataset.name == scan_saving.dataset_name
    assert not dataset.has_scans
    assert len(list(dataset.scan_nodes)) == 0
    assert dataset.proposal.node.db_name == proposal.node.db_name
    assert dataset.collection.node.db_name == collection.node.db_name

    loopscan(3, 0.01, diode)

    assert dataset.has_scans
    assert len(list(dataset.scan_nodes)) == 1

    assert dataset.path.startswith(proposal.path)
    assert dataset.path.startswith(collection.path)
    assert collection.path.startswith(proposal.path)
    assert len(dataset.path) > len(proposal.path)
    assert len(dataset.path) > len(collection.path)
    assert len(collection.path) > len(proposal.path)


def test_dataset_object(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    # Prepare for scanning without the Nexus writer
    diode = session.env_dict["diode"]
    scan_saving.writer = "hdf5"

    # First dataset
    s = loopscan(3, 0.01, diode)

    dataset = scan_saving.dataset
    assert dataset.has_scans
    assert dataset.has_data
    assert [s.name for s in dataset.scan_nodes] == [s.node.name]
    assert not dataset.is_closed

    # Second dataset
    info = icat_test_utils.dataset_info(scan_saving)
    scan_saving.dataset_name = None
    assert dataset.is_closed
    assert scan_saving._dataset_object is None
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    s = loopscan(3, 0.01, diode)
    assert scan_saving.dataset.node.db_name != dataset.node.db_name

    # Third dataset
    info = icat_test_utils.dataset_info(scan_saving)
    scan_saving.dataset_name = None
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    # Third dataset not in Redis yet
    n = get_node(session.name)
    walk_res = [d for d in n.walk(wait=False, include_filter="dataset")]
    assert len(walk_res) == 2

    s = loopscan(3, 0.01, diode, save=False)

    # Third dataset in Redis
    n = get_node(session.name)
    walk_res = [d for d in n.walk(wait=False, include_filter="dataset")]
    assert len(walk_res) == 3

    # Third dataset object does not exist yet
    assert scan_saving._dataset_object is None

    # Third dataset created upon using it
    dataset = scan_saving.dataset
    assert not dataset.is_closed
    assert dataset.has_scans
    assert not dataset.has_data
    assert [s.name for s in dataset.scan_nodes] == [s.node.name]

    # Does not go to a new dataset (because the current one has no data)
    scan_saving.dataset_name = None

    # Still in third dataset
    assert scan_saving.dataset.node.db_name == dataset.node.db_name
    assert not dataset.is_closed
    assert dataset.has_scans
    assert not dataset.has_data
    assert [s.name for s in dataset.scan_nodes] == [s.node.name]
    assert icat_mock_client.return_value.store_dataset.call_count == 0

    # Test walk on datasets
    n = get_node(session.name)
    walk_res = [d for d in n.walk(wait=False, include_filter="dataset")]
    assert len(walk_res) == 3


@pytest.mark.parametrize("missing_dataset", (True, False))
@pytest.mark.parametrize("missing_collection", (True, False))
@pytest.mark.parametrize("missing_proposal", (True, False))
@pytest.mark.parametrize("policymethod", (newdataset, newsample, newproposal))
def test_missing_icat_nodes_not_blocking(
    missing_dataset,
    missing_collection,
    missing_proposal,
    policymethod,
    session,
    esrf_data_policy,
    nexus_writer_service,
):
    diode = session.config.get("diode")
    scan_saving = session.scan_saving

    s = loopscan(1, 0.001, diode)

    old_dataset = scan_saving.dataset
    assert not old_dataset.is_closed

    if missing_dataset:
        dataset = scan_saving.dataset.node
        s.root_connection.delete(dataset.db_name)
    if missing_collection:
        collection = scan_saving.collection.node
        s.root_connection.delete(collection.db_name)
    if missing_proposal:
        proposal = scan_saving.proposal.node
        s.root_connection.delete(proposal.db_name)

    # Check that the dataset is closed despite the exception
    # due to missing nodes (i.e. incomplete ICAT metadata)
    if missing_dataset or missing_collection or missing_proposal:
        with pytest.raises(
            RuntimeError,
            match="The dataset was closed but its ICAT metadata is incomplete",
        ):
            policymethod("new")
        assert old_dataset.is_closed

    # Check that the session is not blocked
    policymethod("new")


def test_data_policy_db_names(session, esrf_data_policy):
    diode = session.config.get("diode")
    proposal = session.scan_saving.proposal
    collection = session.scan_saving.collection
    dataset = session.scan_saving.dataset
    scan = loopscan(3, 0.01, diode, save=False)
    scan_prefix = scan.node.db_name

    policy_db_names1, data_db_names1 = proposal._get_db_names(include_parents=False)
    policy_db_names2, data_db_names2 = collection._get_db_names(include_parents=False)
    policy_db_names3, data_db_names3 = dataset._get_db_names(include_parents=False)

    assert all(db_name.startswith(scan_prefix) for db_name in data_db_names1)
    assert data_db_names1 == data_db_names2
    assert data_db_names1 == data_db_names3
    assert policy_db_names2 < policy_db_names1
    assert policy_db_names3 < policy_db_names2

    policy_db_names1, data_db_names1 = proposal._get_db_names(include_parents=True)
    policy_db_names2, data_db_names2 = collection._get_db_names(include_parents=True)
    policy_db_names3, data_db_names3 = dataset._get_db_names(include_parents=True)

    assert all(db_name.startswith(scan_prefix) for db_name in data_db_names1)
    assert data_db_names1 == data_db_names2
    assert data_db_names1 == data_db_names3
    assert policy_db_names2 == policy_db_names1
    assert policy_db_names3 == policy_db_names2
