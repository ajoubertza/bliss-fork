import os


def assert_initial_proposal(icat_mock_client, scan_saving):
    assert icat_mock_client.called_once_with(
        beamline=scan_saving.beamline, proposal=scan_saving.proposal_name
    )
    assert icat_mock_client.return_value.start_investigation.called_once_with(
        beamline=scan_saving.beamline, proposal=scan_saving.proposal_name
    )
    icat_mock_client.reset_mock()


def assert_proposal_icat(
    icat_mock_client, beamline, proposal, path, session, bliss_session
):
    icat_mock_client.return_value.start_investigation.assert_called_once_with(
        beamline=beamline, proposal=proposal
    )


def assert_proposal_elog(
    icat_mock_client, beamline, proposal, path, session, bliss_session
):
    icat_mock_client.return_value.send_message.assert_called_once_with(
        f"Proposal set to '{proposal}' (session '{session}')\nData path: {path}",
        beamline_only=None,
        msg_type="info",
        tags=[bliss_session],
    )


def assert_collection_elog(icat_mock_client, proposal, collection, path, bliss_session):
    icat_mock_client.return_value.send_message.assert_called_once_with(
        f"Dataset collection set to '{collection}'\nData path: {path}",
        beamline_only=None,
        msg_type="info",
        tags=[bliss_session],
    )


def assert_dataset_elog(icat_mock_client, proposal, dataset, path, bliss_session):
    icat_mock_client.return_value.send_message.assert_called_once_with(
        f"Dataset set to '{dataset}'\nData path: {path}",
        beamline_only=None,
        msg_type="info",
        tags=[bliss_session],
    )


def assert_dataset_icat(
    icat_mock_client, proposal, dataset, path, bliss_session, metadata=None
):
    assert icat_mock_client.return_value.store_dataset.call_count == 1
    data = icat_mock_client.return_value.store_dataset.call_args[-1]
    assert data["proposal"] == proposal
    assert data["dataset"] == dataset
    assert data["path"] == path
    if metadata:
        for k, v in metadata.items():
            if v is None:
                assert k in data["metadata"], k
            else:
                assert data["metadata"][k] == v, k
    else:
        assert data["metadata"]


def proposal_info(scan_saving):
    return {
        "beamline": scan_saving.beamline,
        "proposal": scan_saving.proposal_name,
        "session": scan_saving.proposal_session_name,
        "path": scan_saving.root_path,
        "bliss_session": scan_saving.session,
    }


def dataset_info(scan_saving):
    return {
        "proposal": scan_saving.proposal_name,
        "dataset": scan_saving.dataset_name,
        "path": scan_saving.root_path,
        "bliss_session": scan_saving.session,
    }


def collection_info(scan_saving):
    return {
        "proposal": scan_saving.proposal_name,
        "collection": scan_saving.collection_name,
        "path": scan_saving.root_path,
        "bliss_session": scan_saving.session,
    }


def create_dataset(scan_saving):
    """Create the dataset on disk and in Bliss without having to run a scan"""
    paths = [scan_saving.root_path, scan_saving.icat_root_path]
    for path in paths:
        if not os.path.exists(path):
            os.makedirs(path)
    assert scan_saving.dataset is not None
