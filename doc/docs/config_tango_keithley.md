# Keithley Tango Device Server #

## Configuration ##
### Tango database ###
The device server depends on the beacon configuration for the corresponding
Keithley device.

There is only one property to be configured, **beacon_name**. The value is the **name** of the controller, communicating with the hardware (e.g.pico7 in
[Keithley](config_keithley.md))

![Screenshot](img/pico7_jive.png)

### Controller ###

The access is done via the  device server, instead of direct communication
with the hardware. Here follows the example of the yaml configuration file:

```yaml
name: pico7_diode
module: keithley
plugin: generic
class: TangoKeithley
tango:
   url: id00/keithley/pico7

```
the *url* is the Tango Device in the form *domain/family/member*

## Launching the device server ##

### On the command line ###
Let's make it easy and see how to launch the device server:

```
user@beamline:~/bliss$ conda activate bliss
(bliss) user@beamline:~/bliss$ export TANGO_HOST=id00:20000
(bliss) user@beamline:~/bliss$ export BEACON_HOST=mybeacon:25000
(bliss) user@beamline:~/bliss$ Multimeter -?
usage :  Multimeter instance_name [-v[trace level]] [-nodb [-dlist <device name list>]]
Instance name defined in database for server Multimeter :
        pico7_ds
(bliss) user@beamline:~/bliss$ Multimeter pico7_diode
Ready to accept request
```
### With supervisor ###
Here follows the example of a file, which serves one keithley. The file
should have the *.conf* extension:
```~blissadm/local/daemon/config/supervisor.d/keitley.conf```

```
[group:KEITHLEY_ID00]
programs=id00_pico7
priority=100

[program:id00_pico7]
command=bash -c "source /users/blissadm/conda/miniconda/bin/activate bliss_dev 
&& exec /users/blissadm/conda/miniconda/envs/bliss_dev/bin/Multimeter pico7_ds"
environment=TANGO_HOST="id00:20000",HOME="/users/blissadm",BEACON_HOST="mybeacon:25000"
user=blissadm
startsecs=2
autostart=true
redirect_stderr=true
stdout_logfile=/var/log/%(program_name)s.log
stdout_logfile_maxbytes=1MB
stdout_logfile_backups=10
stdout_capture_maxbytes=1MB
```