# Keithley 2700 configuration

Keithley 2700 is like the 2000 model with **extra sensors**.
The multimeter 2000 allows only thermocouple sensor temperature type: K, K or T.
The multimeter 2000 allows seveal sensors type:
- temperature type: J, K, N, T, E, R, S or B ;
- resistance temperature detector type: PT100, D100, F100, PT3916 or PT385 ;
- thermistor type in ohms: 1950 to 10050.
## Yaml sample configuration
```YAML
plugin: keithley
keithleys:
  - model: 2000
    gpib:
      # url: enet://gpibcb184a
      url: enet://gpibbcu1      
      pad: 15
    sensors:
      - name: keith2000
        address: 1
        meas_func: VOLT
        meas_func: TEMP
        thermocouple_type: K

  - model: 2700 ## for the keithley 2700
    gpib:
      # url: enet://gpibcb184a
      url: enet://gpibbcu1      
      pad: 16
    sensors:
      - name: keith2700
        address: 1
        meas_func: TEMP
        #thermocouple_type: K
        fourwrtd_type: PT100
        #thermistor_type: 5000
        measurement_resolution: 6
```
