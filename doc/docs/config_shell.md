# BLISS shell

BLISS shell saves its configuration under the `bliss.ini` file,
located in the system user config files directory in a specific
ESRF folder (under Linux: `$HOME/.config/ESRF/bliss.ini`).

!!!note
    If it does not exists, this file is created at Bliss startup.

Some customization options exist, for all sessions ran by the
user.

## Syntax highlighting

Two variables `dark-theme` and `light-theme` can be defined,
to specify which syntax highlighting scheme to use depending
on the terminal background color.

The acceptable values are the ones from default `pygments`
styles.

See <https://pygments.org/demo/> for a demo, with the list of available styles.



