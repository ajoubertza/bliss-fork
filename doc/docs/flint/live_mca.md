# Live MCA

For scans containing MCAs data, the MCA widget will be displayed automatically.

A specific widget will be created per detector.

Only the last retrieved data will be displayed, so for a time scan you have to
create first ROIs on the MCAs to display data in time.

![Flint screenshot](img/flint-mca-widget.png)
