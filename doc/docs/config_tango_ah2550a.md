# AH2550A Device Server #

## Launching the device server ##

Let's make it easy and see how to launch the device server:

```
(base) user@beamline:~/bliss$ conda activate bliss
(bliss) user@beamline:~/bliss$ export TANGO_HOST=localhost:20000
(bliss) user@beamline:~/bliss$ python tango/servers/ah2550a_ds.py -?
usage :  ah2550a_ds instance_name [-v[trace level]] [-nodb [-dlist <device name list>]]
Instance name defined in database for server ah2550a_ds :
        id00
(bliss) user@beamline:~/bliss$ python tango/servers/ah2550a_ds.py id00
Ready to accept request
```

NB: not yet distributed as a separate conda package.


## Configuration ##

### Jive ###

create server
```
Server: ah2550a_ds/id00
Class: AndeenHagerling2550a
Device: id00/ah2550a/ah
properties:
- beacon_name: ah
```

### Not yet With Beacon ###




