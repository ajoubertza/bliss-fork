from bliss.common import cleanup
import gevent
import time
from bliss.setup_globals import mvr, slit_top


def _slit_step(axis):
    mvr(axis, 0.1)


def increment():
    t0 = time.perf_counter()
    with cleanup.cleanup(slit_top, restore_list=(cleanup.axis.POS,)):
        while time.perf_counter() - t0 < 5:
            _slit_step(slit_top)
            gevent.sleep(0.2)
