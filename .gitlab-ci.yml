# Workflow determines rules for the entire pipeline
# here pipeline runs for tags, merge requests, master branch,
# releases branches (like 1.10.x) or when scheduled.
# (i.e. we want to avoid twice CI on merge requests and branches)
workflow:
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_BRANCH =~ /^\d+\.\d+\.x$/
    - if: $CI_PIPELINE_SOURCE == "schedule"

variables:
    PIP_CACHE_DIR: /opt/cache/pip
    PYTHON_VERSION: 3.8*

default:
  image: condaforge/mambaforge
  before_script:
    # /dev/random is super slow
    # https://www.tango-controls.org/community/forum/c/platforms/gnu-linux/device-server-gets-stuck-then-works-as-expected/
    # https://stackoverflow.com/questions/26021181/not-enough-entropy-to-support-dev-random-in-docker-containers-running-in-boot2d
    - rm /dev/random
    - ln -s /dev/urandom /dev/random
    - . /opt/conda/etc/profile.d/conda.sh
    - . /opt/conda/etc/profile.d/mamba.sh
    - conda config --set always_yes true --set quiet true
    - conda config --append channels esrf-bcu
    - conda config --append channels conda-forge/label/pytango_rc
    # Create empty env
    - mamba create --name default_env
    - mamba activate default_env
    - >
      if [[ -z $CI_MERGE_REQUEST_TARGET_BRANCH_NAME ]];then
        export COMPARE_BRANCH_NAME="master"
      else
        export COMPARE_BRANCH_NAME=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      fi

stages:
  - style
  - tests
  - build
  - package_tests
  - deploy

check_style:
  stage: style
  script:
    - mamba install --file requirements-dev.txt python==$PYTHON_VERSION
    - LC_ALL=C.UTF-8 black --check .

check_lint:
  stage: style
  script:
    - mamba install --file requirements-dev.txt python==$PYTHON_VERSION
    # run flake8 on diff between current branch and last common ancestor with master
    - git diff -U0 origin/$COMPARE_BRANCH_NAME...$CI_COMMIT_SHA | flake8 --diff
  # allow failure without impacting the rest of the CI (will show an orange warning in the UI)
  allow_failure: true

.template_test_source:
  stage: tests
  script:
    - echo ${CHANGES}
    - >
      if [ $CI_COMMIT_REF_NAME != 'master' ]; then
        # Check for changes manually to circumvent gitlab-ci limitation (issue #1115)
        echo 'Looking for changes...'
        if ! (git diff --name-only origin/$COMPARE_BRANCH_NAME...$CI_COMMIT_SHA | grep -E "$CHANGES"); then
          echo 'Nothing to do'
          exit 0
        fi
      fi
    # install Xvfb and opengl libraries (needed for test_flint)
    - apt-get update && apt-get -y install xvfb libxi6
    # install conda packages and Bliss
    - echo ${PYTHON_VERSION}
    - mamba install --file requirements.txt --file requirements-test.txt python==$PYTHON_VERSION
    - pip install . --no-deps
    - pip install -e blissdata/ --no-deps
    # create separated env for lima
    - mamba create --name $LIMA_SIMULATOR_CONDA_ENV --file requirements-test-lima.txt
    # Make sure python will not reach source from git
    - mv bliss bliss_
    # run tests on source
    - echo ${PYTEST_ARGS}
    - pytest $PYTEST_ARGS
  variables:
    CHANGES: '\.(py|cfg)$|requirements|gitlab-ci|^(bin|extensions|scripts|spec|tests)/'
    LIMA_SIMULATOR_CONDA_ENV: limasimulatorenv

test_bliss:
  extends: .template_test_source
  variables:
    PYTEST_ARGS: 'tests --ignore tests/nexus_writer --ignore tests/qt'

test_bliss_data:
  extends: .template_test_source
  variables:
    PYTEST_ARGS: 'blissdata'

test_qt:
  extends: .template_test_source
  variables:
    PYTEST_ARGS: 'tests/qt'

test_writer:
  extends: .template_test_source
  variables:
    PYTEST_ARGS: 'tests/nexus_writer'

# Tests for windows are disabled for the moment (only do it manually)
# (they cannot run currently because of (at least) redis version for win64 is limited to v3.2.100)
test_bliss_windows:
  stage: tests
  inherit:
      default: false
  when: manual
  tags:
    - conda
    - win
  variables:
    CONDA_ENV: 'bliss-windows-%CI_JOB_ID%'
  before_script:
    # No permissions to edit the root .condarc file on windows
    # - call conda config --set always_yes true --set quiet true
    - call conda create --yes --quiet -n %CONDA_ENV% python=3.7
    - call conda activate %CONDA_ENV%
    - call conda config --env --append channels esrf-bcu
    - call conda install --yes --quiet --file requirements-win64.txt --file requirements-test-win64.txt python==$PYTHON_VERSION
    - call pip install . --no-deps
    - call pip install -e blissdata/ --no-deps
  script:
    - call pytest tests -x --durations=30
  after_script:
    - call conda deactivate
    - call conda env remove -n %CONDA_ENV%

package:
  stage: build
  tags:
    - conda
    - linux
  before_script:
    - !reference [default, before_script]
    # install opengl libraries (needed to avoid problem with pyopengl dependency)
    - apt-get update && apt-get -y install libgl1-mesa-glx
    # create package env and install conda-build
    - mamba install python=3.7 boa
    # create links to reach prefixed compilers of conda
    #- ln -s /opt/conda/envs/buildenv/bin/x86_64-conda_cos6-linux-gnu-gcc /opt/conda/envs/buildenv/bin/gcc
    #- ln -s /opt/conda/envs/buildenv/bin/x86_64-conda_cos6-linux-gnu-g++ /opt/conda/envs/buildenv/bin/g++
  script:
    # triggering the creation of bliss/release.py file
    - python -c "from setup import generate_release_file;generate_release_file()"
    # creating the meta.yaml file for conda package generation
    - cd scripts
    - python create_recipe.py
    - conda mambabuild . --prefix-length=80  --output-folder=../dist/
    # creating a local conda channel to serve bliss package for next stage
    - cd ..
    - mkdir conda-local-channel conda-local-channel/linux-64
    - cp -r dist/linux-64/*.tar.bz2 conda-local-channel/linux-64/
    - conda index conda-local-channel
  artifacts:
    paths:
      - dist/
      - conda-local-channel/
    expire_in: 7 days
  only:
    - tags

package_windows:
  stage: build
  inherit:
    default: false
  tags:
    - conda
    - win
  variables:
    CONDA_ENV: 'bliss-windows-%CI_JOB_ID%'
  before_script:
    # Create a dedicated env to avoid to pollute the shared machine
    # No permissions to edit the root .condarc file on windows:
    # - call conda config --set always_yes true --set quiet true
    - call conda create --yes --quiet -n %CONDA_ENV% python=3.7 boa
    - call conda activate %CONDA_ENV%
    - call conda config --env --append channels esrf-bcu
  script:
    # triggering the creation of bliss/release.py file
    - python -c "from setup import generate_release_file;generate_release_file()"
    # creating the meta.yaml file for conda package generation
    - cd scripts
    - python create_recipe.py
    - call conda mambabuild . --prefix-length=80  --output-folder=../dist/
    # creating a local conda channel to serve bliss package for next stage
    - cd ..
    - mkdir conda-local-channel conda-local-channel\win-64
    - copy dist\win-64\*.tar.bz2 conda-local-channel\win-64\
    - call conda index conda-local-channel
  after_script:
    - call conda deactivate
    - call conda env remove -n %CONDA_ENV%
  artifacts:
    paths:
      - dist/
      - conda-local-channel/
    expire_in: 7 days
  only:
    - tags

create_reference_doc:
  stage: build
  script:
    # install opengl libraries (needed to avoid problem with pyopengl dependency)
    - apt-get update && apt-get -y install libgl1-mesa-glx
    # install python requirements
    - mamba install --file requirements.txt --file requirements-doc.txt python==$PYTHON_VERSION
    - pip install . --no-deps
    - pip install -e blissdata/ --no-deps
    # build of documentation
    - python setup.py build_sphinx
  artifacts:
    paths:
      - build/
    expire_in: 7 days
  needs: []  # this job has no dependency, will start without waiting for previous stages to complete

create_user_doc:
  stage: build
  script:
    # install opengl libraries (needed to avoid problem with pyopengl dependency)
    - apt-get update && apt-get -y install libgl1-mesa-glx
    # install python requirements
    - mamba install --file requirements-doc.txt python==$PYTHON_VERSION
    # build of documentation (-s : strict : fail on warnings)
    - cd doc && mkdocs build -s
  artifacts:
    paths:
      - doc/site
    expire_in: 7 days
  needs: []  # this job has no dependency, will start without waiting for previous stages to complete

.template_test_package:
  stage: package_tests
  script:
    # install Xvfb and opengl libraries (needed for test_flint)
    - apt-get update && apt-get -y install xvfb libxi6
    - mv bliss source # to avoid import errors (we want to test the package, not local bliss folder
    - mamba install bliss==$CI_COMMIT_TAG --file requirements-test.txt --channel file://${CI_PROJECT_DIR}/conda-local-channel python==$PYTHON_VERSION
    - echo ${PYTEST_ARGS}
    - pytest ${PYTEST_ARGS}

test_bliss_package:
  # Run bliss tests using the bliss conda package
  extends: .template_test_package
  only:
    - tags
  tags:
    - bliss_master
  variables:
    PYTEST_ARGS: 'tests --ignore tests/nexus_writer --ignore tests/qt'

test_bliss_data_package:
  # Run bliss data tests using the bliss conda package
  extends: .template_test_package
  only:
    - tags
  tags:
    - bliss_master
  variables:
    PYTEST_ARGS: 'blissdata'

test_qt_package:
  # Run BLISS tests depending on Qt
  extends: .template_test_package
  only:
    - tags
  tags:
    - bliss_master
  variables:
    PYTEST_ARGS: 'tests/qt'

test_writer_package:
  # Run HDF5 writer tests using the bliss conda package
  extends: .template_test_package
  only:
    - tags
  tags:
    - bliss_master
  variables:
    PYTEST_ARGS: 'tests/nexus_writer'

# pages:
#   stage: deploy
#   before_script:
#     - ''
#   script:
#     # Preparing
#     - git fetch --tags
#     - mkdir -p public
#     # Make a copy of existing documentation on gitlab webpages
#     # Workaround as gitlab does not manage different versions of documentation
#     - apt-get update && apt-get -y install wget
#     - sh scripts/ci/gitlab-ci-docs-publish.sh master $(git tag --sort=-creatordate)
#     # Publishing documentation for the actual version
#     # if we have a tag it will be published under the intranet gitlab page under /tag/ otherwhise under /master/
#     - if [[ ( $CI_COMMIT_REF_NAME == master && -z $CI_COMMIT_TAG ) ]]; then export DOC_DIR='master'; else export DOC_DIR=${CI_COMMIT_TAG}; fi
#     #- rm -rf public/* # target dir should be cleaned at first time
#     - rm -rf public/${DOC_DIR}
#     - mkdir -p public/${DOC_DIR}/api/
#     - cp -rT doc/site public/${DOC_DIR}/
#     - cp -rT build/sphinx/html public/${DOC_DIR}/api/
#   artifacts:
#     paths:
#       - public
#     expire_in: 7 days
#   rules:
#     - if: $CI_PIPELINE_SOURCE == "schedule"
#       when: never
#     - if: $CI_COMMIT_TAG =~ /^\d+\.\d+\.\d+$/ || $CI_COMMIT_BRANCH == "master"
#   needs: ['create_reference_doc', 'create_user_doc']

# deploy_bliss:
#   stage: deploy
#   before_script:
#     - ''
#   tags:
#     - conda
#     - linux
#   script:
#     # for tags with 'rc' the package will be published to http://bcu-ci.esrf.fr/stable/
#     # for other tags the package will be published to http://bcu-ci.esrf.fr/stable/
#     - if [[ $COMMIT_TAG == *rc* ]]; then export BASEDIR='/conda-devel'; else export BASEDIR='/conda'; fi
#     - cp -r dist/linux-64/*.tar.bz2 ${BASEDIR}/linux-64/
#     - cp -r dist/win-64/*.tar.bz2 ${BASEDIR}/win-64/
#     # Updating conda package index metadata
#     - conda index ${BASEDIR}
#   rules:
#     - if: $CI_PIPELINE_SOURCE == "schedule"
#       when: never
#     - if: $CI_COMMIT_TAG =~ /^\d+\.\d+\.\d+$/
