# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""Blissdata package

.. autosummary::
    :toctree:

    common
    data
"""

__version__ = "0.1.2"
