# CHANGELOG.md

## 0.2.0 (unreleased)

Added:
   - gevent is optional (use threads by default)

## 0.1.2

Bug fixes:
   - pytango is optional

## 0.1.1

Bug fixes:
   - yaml config URL parsing fails for filename on Windows

## 0.1.0

Added:
   - Access to all data produced by Bliss acquisitions (Redis database 1)
   - Access to all public device settings (Redis database 0)
   - Access to beamline configuration (Beacon server)

The Redis related code has been extracted from the *Bliss* project without
changing the API.
