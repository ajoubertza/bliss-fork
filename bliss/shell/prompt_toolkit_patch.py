# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from pkg_resources import packaging
from prompt_toolkit import application, __version__

_PROMPT_TOOLKIT_HANDLES_SIGINT = packaging.version.parse(
    __version__
) > packaging.version.parse("3.0.24")

if _PROMPT_TOOLKIT_HANDLES_SIGINT:
    # Prevent prompt toolkit to handle SIGINT by default
    class Application(application.Application):
        async def run_async(self, **kwargs):
            kwargs["handle_sigint"] = False
            return await super().run_async(**kwargs)

    application.Application = Application
    application.application.Application = Application
