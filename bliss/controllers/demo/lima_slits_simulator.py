import os
import numpy
import redis
import tango

from bliss.common import greenlet_utils
from Lima import Simulator as LimaSimuMod
import Lima.Server.camera.Simulator as TangoSimuMod


#
# This is a plugin loaded by LimaCCDs
# As BLISS patch subprocess, this have to be reverted
# To make LimaCCDs working
#

greenlet_utils.unpatch(subprocess=True)


class LimaSlitsSimulator(LimaSimuMod.Camera):
    def __init__(self):
        LimaSimuMod.Camera.__init__(self)
        self.redis_conn = None
        self._axes = ()
        self._axes_pos = {}
        self._loaded = False

    def init_img(self):
        """Initialize resources.

        It is not done during object construction to speed up
        the device initialization.
        """
        if self._loaded:
            return
        self._loaded = True
        self.img = numpy.load(self._beam_filename)
        self.img = self.img.astype(numpy.uint32)

    def set_axis_names(self, slit_top, slit_bottom):
        self._axes = (slit_top, slit_bottom)

    def set_beam_filename(self, beam_filename):
        self._beam_filename = beam_filename

    def fillData(self, data):
        """
        Callback function from the simulator
        """
        self.init_img()

        self.redis_conn = redis.Redis(
            host="localhost", port=int(os.environ["BEACON_REDIS_PORT"])
        )

        for axis in self._axes:
            pos = self.redis_conn.hget(f"axis.{axis}", "dial_position")
            try:
                self._axes_pos[axis] = float(pos)
            except TypeError:
                self._axes_pos[axis] = 0.0

        print(" ".join(f"{name}={pos}" for name, pos in self._axes_pos.items()))
        self.redraw(
            data.buffer, self._axes_pos["slit_top"], self._axes_pos["slit_bottom"]
        )

    def redraw(self, data, top, bottom):
        center = 484
        top = center - int(top * 100) - 20
        bottom = int(bottom * 100) + center + 20
        if top > self.img.shape[0]:
            top = self.img.shape[0]
        if top < 0:
            top = 0
        if bottom > self.img.shape[0]:
            bottom = self.img.shape[0]
        if bottom < 0:
            bottom = 0

        print("self.img", numpy.sum(self.img))
        numpy.copyto(data, self.img)
        data += (numpy.random.rand(self.img.shape[1], self.img.shape[1]) * 20).astype(
            numpy.uint32
        )
        data[bottom:] = 0
        data[:top] = 0


class SlitsSimulator(TangoSimuMod.Simulator):
    def init_device(self):
        TangoSimuMod.Simulator.init_device(self)
        self._SimuCamera.set_axis_names(self.slit_top, self.slit_bottom)
        self._SimuCamera.set_beam_filename(self.beam_filename)


class SlitsSimulatorClass(TangoSimuMod.SimulatorClass):
    #    Device Properties
    device_property_list = {
        "slit_top": [tango.DevString, "slit_top_blade", []],
        "slit_bottom": [tango.DevString, "slit_bottom_blade", []],
        "beam_filename": [tango.DevString, "filename containing the beam shape", []],
    }
    device_property_list.update(TangoSimuMod.SimulatorClass.device_property_list)


def get_control(**kwargs):
    return TangoSimuMod.get_control(
        **kwargs, _Simulator=SlitsSimulator, _Camera=LimaSlitsSimulator
    )


def get_tango_specific_class_n_device():
    return SlitsSimulatorClass, SlitsSimulator
